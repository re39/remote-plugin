<?php

namespace Giveandgo\Remote;

use App;
use Backend;
use Giveandgo\Remote\Classes\ClientApi;
use October\Rain\Exception\ApplicationException;
use October\Rain\Exception\ValidationException;
use System\Classes\PluginBase;
use ErrorException;

/**
 * remote Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'remote',
            'description' => 'No description provided yet...',
            'author'      => 'giveandgo',
            'icon'        => 'icon-leaf',
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        $this->registerConsoleCommand('remote.apitest', 'Giveandgo\Remote\Console\ApiTest');
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        // App::fatal(function ($exception) {
        //     tracelog('FATAL');
        //     $report = ClientApi::projects([
        //         'message' => $exception->getMessage(),
        //         'type'    => 'fatal',
        //     ])->report()->post();
        // });
        App::error(function (ValidationException $exception) {
            // Handle the exception...
        });
        App::error(function (ApplicationException $exception) {
            // Handle the exception...
        });
        App::error(function (ErrorException $exception) {
            try {
                $report = ClientApi::projects([
                    'message' => $exception->getMessage(),
                ])->report()->post();
            } catch (ApplicationException $e) {
                tracelog($exception);
                tracelog($e);
            }
        });
        // App::error(function (ExceptionBase $exception) {
        //         $report = ClientApi::projects([
        //             'message' => $exception->getMessage(),
        //         ])->report()->post();
        //     }
        // });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Giveandgo\Remote\Components\Logger' => 'logger',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {

        return [
            'giveandgo.remote.general' => [
                'tab'   => 'remote',
                'label' => 'General access',
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'api' => [
                'label'       => 'Remote API',
                'description' => 'Управление настройками Give&GO API',
                'category'    => 'Give&GO',
                'icon'        => 'icon-cog',
                'class'       => '\Giveandgo\Remote\Models\ApiSettings',
                'order'       => 500,
                'keywords'    => 'API api_key api_secret',
                'permissions' => ['giveandgo.remote.*'],
            ],
        ];
    }
}
