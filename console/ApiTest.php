<?php
namespace Giveandgo\Remote\Console;

use Giveandgo\Remote\Classes\ClientApi;
use Illuminate\Console\Command;

class ApiTest extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'remote:apitest';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $report = ClientApi::projects([
            'message' => 'Report test',
        ])->report()->post();
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
