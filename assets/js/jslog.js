+function(w){

    w.errorLogger = w.errorLogger || {}

    /*
     * @param err {String} - error message
     * @param url {String} - url, not used
     * @param line {integer} - line number
     */
    errorLogger.traceError = function(err, url, line) {

        var data =  {
            errorMsg: err,
            errorLine: line,
            queryString: document.location.search,
            url: document.location.pathname,
            referrer: document.referrer,
            userAgent: navigator.userAgent
        }

        var requestData =  Object.entries(data).map(e => e.join('=')).join('&');

        if (!Object.entries) {
            Object.entries = function( obj ){
                var ownProps = Object.keys( obj ),
                    i = ownProps.length,
                    resArray = new Array(i); // preallocate the Array
                while (i--)
                  resArray[i] = [ownProps[i], obj[ownProps[i]]];

                return resArray;
            };
        }

        var requestHeaders = {
            'X-OCTOBER-REQUEST-HANDLER': "onAjaxLog",
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/x-www-form-urlencoded'
        }

        var xhr = new XMLHttpRequest()

        xhr.open("post", window.location, true)

        var headerKeys = Object.keys(requestHeaders)
        for(var i = 0;i<headerKeys.length;i++) {
            xhr.setRequestHeader(headerKeys[i], requestHeaders[headerKeys[i]])
        }

        xhr.send(requestData)

        return false
    }

    window.onerror = errorLogger.traceError

}(window)