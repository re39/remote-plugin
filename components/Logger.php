<?php

namespace Giveandgo\Remote\Components;

use Cms\Classes\ComponentBase;
use Giveandgo\Remote\Classes\ClientApi;
use Input;
use Log;

/**
 * Logger
 */
class Logger extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'casesItem Component',
            'description' => 'No description provided yet...',
        ];
    }

    public function onRun()
    {
        $this->addJs('/plugins/giveandgo/remote/assets/js/jslog.js');
    }

    public function onAjaxLog()
    {

        $input = Input::get();

        $message = 'Javascript error: ' . array_get($input, 'errorMsg', 'empty') . PHP_EOL;
        $message .= print_r($input, true);

        Log::error($message, $input);

        $report = ClientApi::projects([
            'message' => $message,
        ])->report()->post();
    }
}
