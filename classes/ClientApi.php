<?php

namespace Giveandgo\Remote\Classes;

use Cache;
use Giveandgo\Remote\Models\ApiSettings;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

class ClientApi
{
    use \October\Rain\Support\Traits\Singleton;

    /**
     * @var mixed
     */
    protected $api_key = null;

    /**
     * @var mixed
     */
    protected $secret_key = null;

    /**
     * @var mixed
     */
    protected $hash = null;

    /**
     * @var mixed
     */
    protected $token = null;

    /**
     * @var mixed
     */
    protected $resource = null;

    /**
     * @var mixed
     */
    protected $action = null;

    /**
     * @var array
     */
    protected $params = [];

    /**
     * @var mixed
     */
    protected $ch = null;

    public function __construct()
    {
        $this->api_key    = ApiSettings::get('api_key');
        $this->secret_key = ApiSettings::get('secret_key');
        $this->hash       = hash_hmac("sha512", $this->api_key, $this->secret_key);
        $this->token      = Cache::get('api_token');

        if (!$this->token) {
            $this->login();
        }
    }

    /**
     * @return mixed
     */
    public function login()
    {

        if (app()->env !== 'production') {
            return;
        }

        $params = [
            'api_key' => $this->api_key,
            'hash'    => $this->hash,
        ];

        try {
            $response = $this->call('login', $params);
        } catch (\Exception $e) {
            throw new \ApplicationException("API login failed: " . $e->getMessage(), 1);
        }

        $this->token = $response->token;

        Cache::put('api_token', $this->token, 30);
    }

    /**
     * Handle dynamic static method calls into the method.
     *
     * @param  string  $method
     * @param  array   $parameters
     * @return mixed
     */
    public static function __callStatic($method, $parameters)
    {
        return static::instance()->$method(...$parameters);
    }

    /**
     * Handle dynamic method calls into the API.
     *
     * @param  string  $method
     * @param  array   $parameters
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        foreach ($parameters as $key => $value) {
            if (is_array($value)) {
                $this->params = array_merge($this->params, $value);
            }
        }

        if (!$this->resource) {
            $this->resource = $method;
        } elseif (!$this->action) {
            $this->action = $method;
        } else {
            throw new \ApplicationException("Too many api methods called", 1);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return $this->call($this->resource, $this->params, $this->action);
    }

    /**
     * @return mixed
     */
    public function post()
    {
        return $this->call($this->resource, $this->params, $this->action, 'post');
    }

    protected function clear()
    {
        $this->resource = null;
        $this->action   = null;
        $this->params   = [];
    }

    protected function call($resource, $params = [], $action = null, $method = 'get')
    {
        if (app()->env !== 'production') {
            return;
        }

        $this->clear();

        if ($resource != 'login' && !$this->token) {
            throw new \ApplicationException("You have not called the login function with your private and public keys!", 1);
        }

        // Remove trailing slash from api_url if it exists
        $apiUrl = rtrim(ApiSettings::get('api_url'), '/');

        // Construct base URI
        $client = new Client(['base_uri' => $apiUrl . '/api/v1/']);
        $url    = $resource . ($action ? '/' . $action : '');

        // Set headers including Authorization
        $headers = [
            'charset'       => 'utf-8',
            'Authorization' => 'Bearer ' . $this->token,
        ];

        // Prepare request options
        $options = ['headers' => $headers];

        if ($method === 'get') {
            $options['query'] = $params;
        } else {
            $options['form_params'] = $params;
        }

        try {
            $response = $client->request($method, $url, $options);

            // Ensure response status is 2xx
            $statusCode = $response->getStatusCode();
            if ($statusCode < 200 || $statusCode >= 300) {
                throw new \ApplicationException("Unexpected HTTP status code: $statusCode", 1);
            }

            // Decode JSON response directly
            $data = json_decode($response->getBody()->getContents(), false, 512, JSON_THROW_ON_ERROR);

            if (is_object($data)) {
                return $data;
            }

            throw new \ApplicationException('Unexpected response format: Expected JSON object', 1);

        } catch (ClientException | ServerException $e) {
            // Handle 4xx and 5xx HTTP errors
            $statusCode = $e->getResponse()->getStatusCode();
            $errorBody  = $e->getResponse()->getBody()->getContents();
            tracelog([
                $apiUrl,
                $url,
                $method,
                $statusCode,
                $errorBody,
            ]);
            throw new \ApplicationException("HTTP error $statusCode", 1);
        } catch (RequestException $e) {
            // Handle other request exceptions
            throw new \ApplicationException('Guzzle error: ' . $e->getMessage(), 1);
        } catch (\JsonException $e) {
            // Handle JSON decoding errors
            throw new \ApplicationException('JSON decoding error: ' . $e->getMessage(), 1);
        }
    }
}
