<?php
namespace Giveandgo\Remote\Models;

use Model;

/**
 * ApiSettings Model
 */
class ApiSettings extends Model
{

    /**
     * @var array
     */
    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    /**
     * @var string
     */
    public $settingsCode = 'giveandgo_api_settings';

    // Reference to field configuration
    /**
     * @var string
     */
    public $settingsFields = 'fields.yaml';
}
